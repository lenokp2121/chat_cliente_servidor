
package chatcliente.msg;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author lenok
 */
public class ChatMessage implements Serializable{
    
    private String name;
    private String text;
    private String nomeReservado;
    private Set<String> setOnlines = new HashSet<String>();
    private Action action;
    
    
     /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the nameReservado
     */
    public String getNomeReservado() {
        return nomeReservado;
    }

    /**
     * @param nameReservado the nameReservado to set
     */
    public void setNomeReservado(String nameReservado) {
        this.nomeReservado = nameReservado;
    }

    /**
     * @return the setOnlines
     */
    public Set<String> getSetOnlines() {
        return setOnlines;
    }

    /**
     * @param setOnlines the setOnlines to set
     */
    public void setSetOnlines(Set<String> setOnlines) {
        this.setOnlines = setOnlines;
    }

    /**
     * @return the action
     */
    public Action getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(Action action) {
        this.action = action;
    }
    
    public enum Action{
        CONNECT, DISCONNECT, SEND_ONE, SEND_ALL, USERS_ONLINE
    }


}
