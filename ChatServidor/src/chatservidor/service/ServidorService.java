package chatservidor.service;

import chatcliente.msg.ChatMessage;
import chatcliente.msg.ChatMessage.Action;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lenok
 */
public class ServidorService {

    private ServerSocket serverSocket;
    private Socket socket;
    private Map<String, ObjectOutputStream> mapOnlines = new HashMap<String, ObjectOutputStream>();

    public ServidorService() {

        try {
            serverSocket = new ServerSocket(5555);

            System.out.println("Servidor On!");

            while (true) {
                socket = serverSocket.accept();

                new Thread(new ListenerSocket(socket)).start();
            }

        } catch (IOException ex) {
            Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private class ListenerSocket implements Runnable {

        private ObjectOutputStream output;
        private ObjectInputStream input;

        public ListenerSocket(Socket socket) {

            try {
                this.output = new ObjectOutputStream(socket.getOutputStream());
                this.input = new ObjectInputStream(socket.getInputStream());
            } catch (IOException ex) {
                Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        @Override
        public void run() {

            ChatMessage message = null;

            try {
                while ((message = (ChatMessage) input.readObject()) != null) {
                    Action action = message.getAction();

                    if (action.equals(Action.CONNECT)) {
                        boolean isConnect = connect(message, output);
                        if (isConnect) {
                            mapOnlines.put(message.getName(), output);
                            sendOnlines();
                        }

                        System.out.println("Cliente " + message.getName() + " Conectado!");

                    } else if (action.equals(Action.DISCONNECT)) {
                        disconnect(message, output);
                        sendOnlines();
                        return;
                    } else if (action.equals(Action.SEND_ONE)) {
                        sendOne(message);
                    } else if (action.equals(Action.SEND_ALL)) {
                        sendAll(message);
                    }

                }
            } catch (IOException ex) {
                ChatMessage ms = new ChatMessage();
                ms.setName(message.getName());
                disconnect(ms, output);
                sendOnlines();
                System.out.println(ms.getName() + " deixou o chat.");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        private void disconnect(ChatMessage message, ObjectOutputStream output) {
            mapOnlines.remove(message.getName());

            message.setText(" deixou o chat!");
            
            message.setAction(Action.SEND_ONE);
            
            sendAll(message);

        }

        private void sendAll(ChatMessage message) {

            for (Map.Entry<String, ObjectOutputStream> kv : mapOnlines.entrySet()) {
                if (!kv.getKey().equals(message.getName())) {
                    message.setAction(Action.SEND_ONE);
                    try {
                        kv.getValue().writeObject(message);
                    } catch (IOException ex) {
                        Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        }

    }

    private boolean connect(ChatMessage message, ObjectOutputStream output) {

        if (mapOnlines.size() == 0) {
            message.setText("YES");
            send(message, output);
            return true;
        }


            if (mapOnlines.containsKey(message.getName())) {
                message.setText("NO");
                send(message, output);
                return false;
            } else {
                message.setText("YES");
                send(message, output);
                return true;
            }

    }

    //Utilizado pelo servidor!
    private void send(ChatMessage message, ObjectOutputStream output) {
        try {
            output.writeObject(message);
        } catch (IOException ex) {
            Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //faz o loop nos usuarios e envia a mensagem so para o usuario selecionado.
    private void sendOne(ChatMessage message) {

        for (Map.Entry<String, ObjectOutputStream> kv : mapOnlines.entrySet()) {
            if (kv.getKey().equals(message.getNomeReservado())) {
                try {
                    kv.getValue().writeObject(message);
                } catch (IOException ex) {
                    Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    //atualiza a lista de usuarios online.
    private void sendOnlines() {

        Set<String> setNomes = new HashSet<String>();

        for (Map.Entry<String, ObjectOutputStream> kv : mapOnlines.entrySet()) {
            setNomes.add(kv.getKey());
        }

        ChatMessage message = new ChatMessage();
        message.setAction(Action.USERS_ONLINE);
        message.setSetOnlines(setNomes);

        for (Map.Entry<String, ObjectOutputStream> kv : mapOnlines.entrySet()) {
            message.setName(kv.getKey());
            try {
                System.out.println(kv.getKey());
                kv.getValue().writeObject(message);
            } catch (IOException ex) {
                Logger.getLogger(ServidorService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
